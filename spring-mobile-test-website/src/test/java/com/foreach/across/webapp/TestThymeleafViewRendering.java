package com.foreach.across.webapp;

import com.foreach.across.modules.spring.mobile.test.SpringMobileApplication;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Arne Vandamme
 */
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
@TestPropertySource(properties = { "springMobile.deviceResolverRegistration=INTERCEPTOR",
                                   "across.web.views.jsp.enabled=true" })
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SpringMobileApplication.class)
public class TestThymeleafViewRendering
{
	public static final String IPHONE_UA =
			"Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.3 (KHTML, like Gecko) Version/8.0 Mobile/12A4345d Safari/600.1.4";

	public static final String IPAD_UA =
			"Mozilla/5.0 (iPad; CPU OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465 Safari/9537.53";

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@Before
	public void initMvc() {
		mockMvc = MockMvcBuilders.webAppContextSetup( wac ).build();
	}

	@Test
	public void desktopRequest() throws Exception {
		mockMvc.perform( get( "/" ) )
		       .andExpect( status().isOk() )
		       .andDo( mvcResult -> {
			       String content = mvcResult.getResponse().getContentAsString();

			       assertTrue( StringUtils.contains( content, "template type: thymeleaf" ) );
			       assertTrue( StringUtils.contains( content, "template: normal" ) );
			       assertTrue( StringUtils.contains( content, "currentDevice: NORMAL" ) );
			       assertTrue( StringUtils.contains( content, "deviceType: normal" ) );
			       assertTrue( StringUtils.contains( content, "include: normal" ) );
		       } );
	}

	@Test
	public void mobileRequest() throws Exception {
		mockMvc.perform( get( "/" ).header( "User-Agent", IPHONE_UA ) )
		       .andExpect( status().isOk() )
		       .andDo( mvcResult -> {
			       String content = mvcResult.getResponse().getContentAsString();

			       assertTrue( StringUtils.contains( content, "template type: thymeleaf" ) );
			       assertTrue( StringUtils.contains( content, "template: mobile" ) );
			       assertTrue( StringUtils.contains( content, "currentDevice: MOBILE" ) );
			       assertTrue( StringUtils.contains( content, "deviceType: mobile" ) );
			       assertTrue( StringUtils.contains( content, "include: normal" ) );
		       } );
	}

	@Test
	public void tabletRequest() throws Exception {
		mockMvc.perform( get( "/" ).header( "User-Agent", IPAD_UA ) )
		       .andExpect( status().isOk() )
		       .andDo( mvcResult -> {
			       String content = mvcResult.getResponse().getContentAsString();

			       assertTrue( StringUtils.contains( content, "template type: thymeleaf" ) );
			       assertTrue( StringUtils.contains( content, "template: tablet" ) );
			       assertTrue( StringUtils.contains( content, "currentDevice: TABLET" ) );
			       assertTrue( StringUtils.contains( content, "deviceType: tablet" ) );
			       assertTrue( StringUtils.contains( content, "include: normal" ) );
		       } );
	}

	@Test
	public void normalRequestDialect() throws Exception {
		mockMvc.perform( get( "/dialect" ) )
		       .andExpect( status().isOk() )
		       .andDo( mvcResult -> {
			       String content = mvcResult.getResponse().getContentAsString();
			       assertTrue( StringUtils.contains( content, "include: normal" ) );
		       } );
	}

	@Test
	public void phoneRequestDialect() throws Exception {
		mockMvc.perform( get( "/dialect" ).header( "User-Agent", IPHONE_UA ) )
		       .andExpect( status().isOk() )
		       .andDo( mvcResult -> {
			       String content = mvcResult.getResponse().getContentAsString();
			       assertTrue( StringUtils.contains( content, "include: mobile" ) );
		       } );
	}

	@Test
	public void tabletRequestDialect() throws Exception {
		mockMvc.perform( get( "/dialect" ).header( "User-Agent", IPAD_UA ) )
		       .andExpect( status().isOk() )
		       .andDo( mvcResult -> {
			       String content = mvcResult.getResponse().getContentAsString();
			       assertTrue( StringUtils.contains( content, "include: tablet" ) );
		       } );
	}
}
