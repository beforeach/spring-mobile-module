package com.foreach.across.modules.spring.mobile.test.application.controllers;

import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Arne Vandamme
 */
@Controller
public class PageController
{
	@RequestMapping({ "/", "/thymeleaf" })
	public String thymleafPage( Device device, ModelMap model ) {
		String name = "normal";
		if ( device.isMobile() ) {
			name = "mobile";
		}
		else if ( device.isTablet() ) {
			name = "tablet";
		}
		model.addAttribute( "deviceTypeName", name );

		return "th/testweb/page";
	}

	@RequestMapping("/dialect")
	public String deviceDialectTest( Device device, ModelMap model ) {
		String name = "normal";
		return "th/testweb/test";
	}

	@RequestMapping("/jsp")
	public String jspPage() {
		return "demo";
	}
}
