package com.foreach.across.modules.spring.mobile.test;

import com.foreach.across.config.AcrossApplication;
import com.foreach.across.modules.debugweb.DebugWebModule;
import com.foreach.across.modules.spring.mobile.SpringMobileModule;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;

/**
 * @author Sander Van Loock
 * @since 1.0.1
 */
@AcrossApplication(modules = SpringMobileModule.NAME)
public class SpringMobileApplication
{
	public static void main( String args[] ) {
		SpringApplication.run( SpringMobileApplication.class, args );
	}

	@Bean
	public DebugWebModule debugWebModule() {
		return new DebugWebModule();
	}
}
