<%@ page session="false" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Demo JSP page</title>
</head>
<body>
<p>Simple webapplication demonstrating Spring Mobile integration with both JSP and Thymeleaf rendering.</p>
<p>
	<a href="thymeleaf">Thymeleaf</a>
</p>
<h2>Normal template, your device is ${currentDevice}.</h2>

</body>
</html>