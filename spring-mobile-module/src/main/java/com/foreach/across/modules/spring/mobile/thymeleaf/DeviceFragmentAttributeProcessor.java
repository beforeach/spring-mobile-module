package com.foreach.across.modules.spring.mobile.thymeleaf;

import com.foreach.across.modules.spring.mobile.services.DeviceBasedViewNameResolver;
import org.springframework.context.ApplicationContext;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.standard.StandardDialect;
import org.thymeleaf.standard.processor.StandardReplaceTagProcessor;
import org.thymeleaf.templatemode.TemplateMode;

public class DeviceFragmentAttributeProcessor extends AbstractAttributeTagProcessor
{
	public DeviceFragmentAttributeProcessor() {
		super( TemplateMode.HTML, DeviceDialect.PREFIX, null, false, StandardReplaceTagProcessor.ATTR_NAME, true,
		       StandardReplaceTagProcessor.PRECEDENCE + 1, true );
	}

	/**
	 * This method replaces the attribute device:replace="XXX" with th:replace"DEVICE_PREFIX/XXX"
	 * This is useful for including device specific fragments
	 */
	@Override
	protected void doProcess( ITemplateContext context,
	                          IProcessableElementTag tag,
	                          AttributeName attributeName,
	                          String attributeValue,
	                          IElementTagStructureHandler structureHandler ) {
		ApplicationContext applicationContext
				= (ApplicationContext) context.getConfiguration()
				                              .getExecutionAttributes()
				                              .get( DeviceDialect.APPLICATION_CONTEXT_ATTRIBUTE );
		DeviceBasedViewNameResolver deviceBasedViewNameResolver
				= applicationContext.getBean( DeviceBasedViewNameResolver.class );

		structureHandler.setAttribute(
				StandardDialect.PREFIX + ":" + StandardReplaceTagProcessor.ATTR_NAME,
				deviceBasedViewNameResolver.resolveDeviceSpecificView( attributeValue )
		);
	}
}
