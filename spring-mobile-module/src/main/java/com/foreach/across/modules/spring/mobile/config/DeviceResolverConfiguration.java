package com.foreach.across.modules.spring.mobile.config;

import com.foreach.across.modules.web.config.support.PrefixingHandlerMappingConfigurer;
import com.foreach.across.modules.web.mvc.InterceptorRegistry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.mobile.device.DeviceResolver;
import org.springframework.mobile.device.DeviceResolverHandlerInterceptor;
import org.springframework.mobile.device.DeviceResolverRequestFilter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Arne Vandamme
 */
@Configuration
@Slf4j
public class DeviceResolverConfiguration
{
	@Configuration
	@ConditionalOnProperty(value = "springMobile.deviceResolverRegistration", havingValue = "FILTER", matchIfMissing = true)
	@RequiredArgsConstructor
	public static class ServletConfiguration
	{
		private final DeviceResolver deviceResolver;

		@Bean
		protected FilterRegistrationBean<DeviceResolverRequestFilter> dynamicConfigurationAllowed() {
			log.info( "Registering DeviceResolverRequestFilter" );

			FilterRegistrationBean<DeviceResolverRequestFilter> deviceResolverRequestFilter =
					new FilterRegistrationBean<>();
			deviceResolverRequestFilter.setFilter( new DeviceResolverRequestFilter( deviceResolver ) );
			deviceResolverRequestFilter.addUrlPatterns( "/*" );

			return deviceResolverRequestFilter;
		}
	}

	@Configuration
	@Order(Ordered.HIGHEST_PRECEDENCE)
	@ConditionalOnProperty(value = "springMobile.deviceResolverRegistration", havingValue = "INTERCEPTOR", matchIfMissing = true)
	@RequiredArgsConstructor
	public static class InterceptorConfiguration implements PrefixingHandlerMappingConfigurer, WebMvcConfigurer
	{
		private final DeviceResolver deviceResolver;

		@Override
		public boolean supports( String mapperName ) {
			log.info( "Registering DeviceResolverHandlerInterceptor to {}", mapperName );
			return true;
		}

		@Override
		public void addInterceptors( org.springframework.web.servlet.config.annotation.InterceptorRegistry registry ) {
			registry.addInterceptor( deviceResolverHandlerInterceptor() );
		}

		@Override
		public void addInterceptors( InterceptorRegistry registry ) {
			registry.addFirst( deviceResolverHandlerInterceptor() );
		}

		@Bean
		public DeviceResolverHandlerInterceptor deviceResolverHandlerInterceptor() {
			return new DeviceResolverHandlerInterceptor( deviceResolver );
		}
	}
}
