package com.foreach.across.modules.spring.mobile.thymeleaf;

import org.springframework.context.ApplicationContext;
import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.dialect.IExecutionAttributeDialect;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.standard.StandardDialect;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DeviceDialect extends AbstractProcessorDialect implements IExecutionAttributeDialect
{
	public static final String PREFIX = "device";
	public static final String APPLICATION_CONTEXT_ATTRIBUTE = "SpringMobileModule.ApplicationContext";

	private final ApplicationContext applicationContext;

	public DeviceDialect( ApplicationContext applicationContext ) {
		super( "DeviceDialect", PREFIX, StandardDialect.PROCESSOR_PRECEDENCE );
		this.applicationContext = applicationContext;
	}

	@Override
	public Map<String, Object> getExecutionAttributes() {
		return Collections.singletonMap( APPLICATION_CONTEXT_ATTRIBUTE, applicationContext );
	}

	@Override
	public Set<IProcessor> getProcessors( String dialectPrefix ) {
		final Set<IProcessor> processors = new HashSet<>();
		processors.add( new DeviceFragmentAttributeProcessor() );
		return processors;
	}
}
