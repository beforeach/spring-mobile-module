package com.foreach.across.modules.spring.mobile;

import com.foreach.across.core.AcrossModule;
import com.foreach.across.core.annotations.AcrossDepends;
import com.foreach.across.modules.web.AcrossWebModule;

/**
 * @author Arne Vandamme
 */
@AcrossDepends(required = AcrossWebModule.NAME)
public class SpringMobileModule extends AcrossModule
{
	public static final String NAME = "SpringMobileModule";

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getDescription() {
		return "Activates Spring Mobile support in a web context.";
	}
}
