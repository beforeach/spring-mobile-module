package com.foreach.across.modules.spring.mobile.services;

import com.foreach.across.modules.spring.mobile.config.SpringMobileConfiguration;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceType;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.EnumSet;
import java.util.Set;

@Service
public class DeviceBasedViewNameResolverImpl implements DeviceBasedViewNameResolver
{
	private Set<DeviceType> deviceTypes = EnumSet.noneOf( DeviceType.class );

	public void setDeviceTypes( Set<DeviceType> deviceTypes ) {
		this.deviceTypes = deviceTypes;
	}

	@Override
	public String resolveDeviceSpecificView( String viewName ) {
		Device device = DeviceUtils.getCurrentDevice( RequestContextHolder.currentRequestAttributes() );
		StringBuilder builder = new StringBuilder();

		if ( device.isMobile() && deviceTypes.contains( DeviceType.MOBILE ) ) {
			builder.append( SpringMobileConfiguration.MOBILE_PREFIX );
		}
		else if ( device.isTablet() && deviceTypes.contains( DeviceType.TABLET ) ) {
			builder.append( SpringMobileConfiguration.TABLET_PREFIX );
		}
		else if ( device.isNormal() && deviceTypes.contains( DeviceType.NORMAL ) ) {
			builder.append( SpringMobileConfiguration.NORMAL_PREFIX );
		}

		builder.append( viewName );

		return builder.toString();
	}
}

