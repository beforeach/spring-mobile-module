package com.foreach.across.modules.spring.mobile.config;

import com.foreach.across.modules.spring.mobile.SpringMobileModuleSettings;
import com.foreach.across.modules.spring.mobile.services.DeviceBasedViewNameResolver;
import com.foreach.across.modules.spring.mobile.services.DeviceBasedViewNameResolverImpl;
import com.foreach.across.modules.spring.mobile.thymeleaf.DeviceDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mobile.device.DeviceHandlerMethodArgumentResolver;
import org.springframework.mobile.device.DeviceResolver;
import org.springframework.mobile.device.DeviceType;
import org.springframework.mobile.device.LiteDeviceResolver;
import org.springframework.mobile.device.view.LiteDeviceDelegatingViewResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thymeleaf.TemplateEngine;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author Arne Vandamme
 */
@Configuration
public class SpringMobileConfiguration implements WebMvcConfigurer
{
	public static final String MOBILE_PREFIX = "mobile/";
	public static final String TABLET_PREFIX = "tablet/";
	public static final String NORMAL_PREFIX = "normal/";

	@Autowired(required = false)
	private Collection<LiteDeviceDelegatingViewResolver> viewResolvers;

	@Autowired
	private SpringMobileModuleSettings settings;

	@Autowired(required = false)
	private TemplateEngine templateEngine;

	@Autowired
	private ApplicationContext applicationContext;

	@PostConstruct
	public void setupModule() {
		if ( templateEngine != null ) {
			templateEngine.addDialect( new DeviceDialect( applicationContext ) );
		}
	}

	@Autowired
	public void registerDeviceTypes() {
		Set<DeviceType> deviceTypes = settings.getDeviceTypes();

		for ( LiteDeviceDelegatingViewResolver resolver : viewResolvers ) {
			if ( deviceTypes.contains( DeviceType.MOBILE ) ) {
				resolver.setMobilePrefix( MOBILE_PREFIX );
			}
			if ( deviceTypes.contains( DeviceType.TABLET ) ) {
				resolver.setTabletPrefix( TABLET_PREFIX );
			}
			if ( deviceTypes.contains( DeviceType.NORMAL ) ) {
				resolver.setNormalPrefix( NORMAL_PREFIX );
			}
		}
	}

	@Override
	public void addArgumentResolvers( List<HandlerMethodArgumentResolver> argumentResolvers ) {
		argumentResolvers.add( deviceHandlerMethodArgumentResolver() );
	}

	@Bean
	public DeviceResolver deviceResolver() {
		return new LiteDeviceResolver();
	}

	@Bean
	public DeviceHandlerMethodArgumentResolver deviceHandlerMethodArgumentResolver() {
		return new DeviceHandlerMethodArgumentResolver();
	}

	@Bean
	public DeviceBasedViewNameResolver deviceBasedViewNameResolver() {
		DeviceBasedViewNameResolverImpl deviceBasedViewNameResolver = new DeviceBasedViewNameResolverImpl();
		deviceBasedViewNameResolver.setDeviceTypes( settings.getDeviceTypes() );
		return deviceBasedViewNameResolver;
	}
}
