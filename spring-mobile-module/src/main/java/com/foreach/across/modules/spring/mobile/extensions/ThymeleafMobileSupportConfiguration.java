package com.foreach.across.modules.spring.mobile.extensions;

import com.foreach.across.core.annotations.Exposed;
import com.foreach.across.core.annotations.ModuleConfiguration;
import com.foreach.across.modules.spring.mobile.config.SpringMobileConfiguration;
import com.foreach.across.modules.web.AcrossWebModule;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.mobile.device.view.LiteDeviceDelegatingViewResolver;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;

/**
 * To be injected in the AcrossWebModule in order to override the default Thymeleaf view resolver to support
 * alternative mobile and tablet views.
 *
 * @author Arne Vandamme
 */
@ModuleConfiguration(value = AcrossWebModule.NAME)
@ConditionalOnProperty(value = "across.web.views.thymeleaf.enabled", matchIfMissing = true)
public class ThymeleafMobileSupportConfiguration
{
	@Bean
	@Exposed
	public ThymeleafViewResolver mobileSupportingThymeleafViewResolver( SpringTemplateEngine springTemplateEngine ) {
		ThymeleafViewResolver resolver = new ThymeleafViewResolver();
		resolver.setTemplateEngine( springTemplateEngine );
		resolver.setOrder( 0 );
		resolver.setCharacterEncoding( "UTF-8" );
		resolver.setViewNames( new String[] {
				"th/*",
				SpringMobileConfiguration.NORMAL_PREFIX + "th/*",
				SpringMobileConfiguration.MOBILE_PREFIX + "th/*",
				SpringMobileConfiguration.TABLET_PREFIX + "th/*",
				ThymeleafViewResolver.REDIRECT_URL_PREFIX + "*",
				ThymeleafViewResolver.FORWARD_URL_PREFIX + "*"
		} );

		return resolver;
	}

	@Bean
	@Exposed
	public LiteDeviceDelegatingViewResolver thymeleafDeviceAwareViewResolver( ThymeleafViewResolver mobileSupportingThymeleafViewResolver ) {
		LiteDeviceDelegatingViewResolver resolver =
				new LiteDeviceDelegatingViewResolver( mobileSupportingThymeleafViewResolver );
		resolver.setOrder( -1 );
		resolver.setEnableFallback( true );

		return resolver;
	}
}
