package com.foreach.across.modules.spring.mobile.services;

import org.springframework.mobile.device.DeviceType;

import java.util.EnumSet;

/**
 * An interface to resolve view names to their correct device-specific variant
 */
public interface DeviceBasedViewNameResolver
{
	/**
	 * Given a view name, this method will return the device specific view name.  The device to use is derived
	 * from the request via {@link org.springframework.mobile.device.DeviceUtils#getCurrentDevice(org.springframework.web.context.request.RequestAttributes)}
	 *
	 * @param viewName The view name to resolve to the correct device
	 * @return The given view name prepended with the correct device prefix
	 */
	String resolveDeviceSpecificView( String viewName );
}
