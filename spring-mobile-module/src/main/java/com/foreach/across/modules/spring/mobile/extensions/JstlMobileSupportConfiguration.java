package com.foreach.across.modules.spring.mobile.extensions;

import com.foreach.across.core.annotations.Exposed;
import com.foreach.across.core.annotations.ModuleConfiguration;
import com.foreach.across.modules.web.AcrossWebModule;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.mobile.device.view.LiteDeviceDelegatingViewResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * @author Arne Vandamme
 */
@ModuleConfiguration(value = AcrossWebModule.NAME)
@ConditionalOnProperty(value = "across.web.views.jsp.enabled")
public class JstlMobileSupportConfiguration
{
	@Bean
	@Exposed
	public LiteDeviceDelegatingViewResolver jstlDeviceAwareViewResolver() {
		LiteDeviceDelegatingViewResolver resolver = new LiteDeviceDelegatingViewResolver( jstlViewResolver() );
		resolver.setOrder( 2 );
		resolver.setEnableFallback( true );

		return resolver;
	}

	@Bean
	public ViewResolver jstlViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix( "/WEB-INF/jsp/" );
		resolver.setSuffix( ".jsp" );
		resolver.setOrder( 2 );
		return resolver;
	}
}
