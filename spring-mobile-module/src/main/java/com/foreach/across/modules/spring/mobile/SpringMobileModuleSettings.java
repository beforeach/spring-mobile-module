package com.foreach.across.modules.spring.mobile;

import com.foreach.across.modules.spring.mobile.config.DeviceResolverRegistration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.mobile.device.DeviceType;

import java.util.EnumSet;
import java.util.Set;

/**
 * @author Arne Vandamme
 */
@ConfigurationProperties(prefix = "spring-mobile")
public class SpringMobileModuleSettings
{
	public static final String DEVICE_RESOLVER_REGISTRATION = "spring-mobile.device-resolver-registration";
	public static final String DEVICE_TYPES = "spring-mobile.device-types";

	private DeviceResolverRegistration deviceResolverRegistration = DeviceResolverRegistration.FILTER;
	private Set<DeviceType> deviceTypes = EnumSet.of( DeviceType.MOBILE, DeviceType.TABLET );

	public DeviceResolverRegistration getDeviceResolverRegistration() {
		return deviceResolverRegistration;
	}

	public void setDeviceResolverRegistration( DeviceResolverRegistration deviceResolverRegistration ) {
		this.deviceResolverRegistration = deviceResolverRegistration;
	}

	public Set<DeviceType> getDeviceTypes() {
		return deviceTypes;
	}

	public void setDeviceTypes( Set<DeviceType> deviceTypes ) {
		this.deviceTypes = deviceTypes;
	}
}
