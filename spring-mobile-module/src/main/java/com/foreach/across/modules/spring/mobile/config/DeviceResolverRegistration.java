package com.foreach.across.modules.spring.mobile.config;

/**
 * @author Arne Vandamme
 */
public enum DeviceResolverRegistration
{
	/**
	 * Create a filter and register it dynamically to all servlets.
	 */
	FILTER,

	/**
	 * Create an interceptor and register on handlermapping level.
	 */
	INTERCEPTOR
}
