package com.foreach.across.modules.spring.mobile.test;

import com.foreach.across.modules.spring.mobile.SpringMobileModule;
import com.foreach.across.modules.spring.mobile.services.DeviceBasedViewNameResolver;
import com.foreach.across.test.AcrossTestConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
@WebAppConfiguration
@ContextConfiguration(classes = ITSpringMobileModule.Config.class)
@TestPropertySource(properties = { "springMobile.deviceResolverRegistration=INTERCEPTOR" })
public class ITSpringMobileModule
{
	@Autowired
	private DeviceBasedViewNameResolver deviceBasedViewNameResolver;

	@Test
	public void exposedBeans() {
		assertNotNull( deviceBasedViewNameResolver );
	}

	@Configuration
	@AcrossTestConfiguration(modules = SpringMobileModule.NAME)
	protected static class Config
	{
	}
}
