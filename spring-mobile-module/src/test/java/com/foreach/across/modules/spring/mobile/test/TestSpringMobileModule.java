package com.foreach.across.modules.spring.mobile.test;

import com.foreach.across.core.AcrossModule;
import com.foreach.across.modules.spring.mobile.SpringMobileModule;
import com.foreach.across.test.AbstractAcrossModuleConventionsTest;

/**
 * @author Arne Vandamme
 */
public class TestSpringMobileModule extends AbstractAcrossModuleConventionsTest
{
	@Override
	protected AcrossModule createModule() {
		return new SpringMobileModule();
	}
}
